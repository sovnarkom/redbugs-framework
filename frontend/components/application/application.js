(function($){
    "use strict";

    var MaterialUI = require('material-ui');

    $(document).ready(function() {

        console.log('jquery ready');

        var stompClient;
        var connected = false;

        function setConnected(aConnected) {
            connected = aConnected;
        }

        function connect() {
            var socket = new SockJS('/websockets/stomp');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function(frame) {
                setConnected(true);
                console.log('Connected: ' + frame);
                stompClient.subscribe('/topic/greetings', function(greeting){
                    showGreeting(greeting.body);
                });
            });
        }

        function disconnect() {
            if (stompClient != null) {
                stompClient.disconnect();
            }
            setConnected(false);
            console.log("Disconnected");
        }

        function sendName() {
            stompClient.send("/app/hello", {}, JSON.stringify({ 'name': "ololololololololloo" }));
        }

        function showGreeting(message) {
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.appendChild(document.createTextNode(message));
            $('body').append(p);
        }

        $('#send-hello').on('click', function() {
            sendName();
        });

        connect();



    });
})(jQuery);