/*!
 * gulp
 */

// Load plugins
var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    path = require('path'),
    wiredep = require('wiredep'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    del = require('del');

function dest(p) {
    return gulp.dest(path.join('..', 'build', 'resources', p));
}

gulp.task('bower', function() {
  return plugins.bower();
});

// Copy Main bower dependencies
gulp.task('bower-main', ['bower'], function() {
  return gulp
    .src('./bower.json')
    .pipe(plugins.mainBowerFiles())
    .pipe(dest('lib'));
});


gulp.task('wiredep', ['bower-main'], function() {
    return gulp
        .src('./components/application/application.html')
        .pipe(plugins.inject(
            gulp
                .src('../build/resources/**')
                .pipe(plugins.order(['lib'])),
            {read: false, ignorePath: '/../build/resources'})
         )
        .pipe(dest('views/application'))
});

//gulp.task('wiredep', ['bower-main'], function() {
//  return gulp.src('./components/application/application.html')
//    .pipe(wiredep.stream({
//        cwd: '../build/resource/lib',
//        directory: './bower_components',
//        ignorePath: '../../bower_components',
//        bowerJson: require('./bower.json'),
//         onPathInjected: function(fileObject) {
//            fileObject.path = 'lib/' + fileObject.path;
//          },
//    }))
//    .pipe(dest('views/application'));
//});

// Styles
gulp.task('views', function() {
  return gulp
    .src('./components/**/*.html')
    .pipe(plugins.processhtml())
    .pipe(dest('views'))
});

// Styles
gulp.task('styles', function() {
  return gulp
    .src('./components/**/*.less')
    .pipe(plugins.less({
            paths: [path.join(__dirname, 'frontend', 'components')]
        }))
    .pipe(dest('styles'))
    //.pipe(plugins.rename({ suffix: '.min' }))
    //.pipe(plugins.minifyCss())
    //.pipe(dest('styles'))
});

// Scripts
gulp.task('scripts', function() {
  return browserify('./components/application/application.js')
    .bundle()
    .pipe(source('./app.js'))
    .pipe(gulp.dest('./dist/js/'));
});

//gulp.task('scripts', function() {
//  return gulp
//    .src('./components/**/*.js')
//    .pipe(plugins.jshint('.jshintrc'))
//    .pipe(plugins.jshint.reporter('default'))
//    //.pipe(concat('main.js'))
//    .pipe(dest('scripts'))
//    //.pipe(plugins.rename({ suffix: '.min' }))
//    //.pipe(plugins.uglify())
//    //.pipe(dest('scripts'))
//});

// Resources
gulp.task('resources', function() {
  return gulp
    .src('./components/**/resources/*.png')
    .pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(dest('resources'))
});

// Clean
gulp.task('clean', function(cb) {
    del(['resources/'], cb)
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('wiredep', 'views', 'styles', 'scripts');
});

// Watch
gulp.task('watch', function() {

  gulp.watch('./components/**/*.less', ['styles']);
  gulp.watch('./components/**/*.js', ['scripts']);
  gulp.watch('./components/**/*.html', ['views', 'wiredep']);
  gulp.watch('./components/**/resources/*.png', ['resources']);

  // Create LiveReload server
  //plugins.livereload.listen();

  // Watch any files in dist/, reload on change
//  gulp.watch('./components/**').on('change', function() {
//    plugins.notify({ message: 'OLOLOLO!' })
//  });

});