package com.somebugs.redbugs.controllers

import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class Index {

    @RequestMapping("/rest")
    def restMethod() {
        return "Hello World! 2";
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    def stompMethod(Map message) {
        [a: message]
    }
}
