# README #

* RedBugs is a framework that can be used to develop applications with gradle/gulp toolchain.
* Note, what is framework: TODO: "framework or library?" article

* For backend we are currently use:
    * Groovy 
    * Spring Boot
    * Project Reactor (events)
    * Gradle

* Frontend is based on:
    * JavaScript
    * LESS
    * React (?)
    * Gulp & Bower
    * GreenBugs (event javascript library TODO: it)

* Version 0.0.1-pre-alpha (initial, not ready to use)

### How do I get set up? ###

TODO:
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

TODO:
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* admin@somebugs.com
* achichein@dataart.com